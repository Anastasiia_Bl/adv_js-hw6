async function getIp() {
    try {
      const ipResponse = await fetch('https://api.ipify.org/?format=json');
      const ipData = await ipResponse.json();
      const ipAddress = ipData.ip;
      return ipAddress;
    } catch (error) {
        console.log(error);
      }
    }

async function getIpLocation(ipAddress) {
    try {
        const locationResponse = await fetch(`http://ip-api.com/json/${ipAddress}?fields=continent,country,regionName,city,district`);
        const locationResult = await locationResponse.json();
        return locationResult;
    } catch(error) {
        console.log(error);
    }
}

async function addIp(locationResult) {
    try {
        const ipInfo = document.querySelector('.ip-info');
        const ul =  document.createElement('ul');

    for (prop in locationResult) {
        const li = document.createElement('li');
        li.innerText = `${prop}: ${locationResult[prop]}`;
        ul.append(li)
    }
        ipInfo.append(ul);
    } catch(error) {
        console.log(error);
    }
}

// слухач доданий у саму функцію
async function showIp() {
    document.querySelector('.ip-btn').addEventListener('click', async () => {
        try {
            const ip = await getIp();
            const location = await getIpLocation(ip);
            const ipList = await addIp(location);
            return ipList;
        } catch(error) {
            console.log(error);
        }
    })
}
showIp();





// слухач поза функцією
// async function showIp() {
//     try {
//         const ip = await getIp();
//         const location = await getIpLocation(ip);
//         const ipList = await addIp(location);
//         return ipList;
//     } catch(error) {
//         console.log(error);
//     }
// }
// document.querySelector('.ip-btn').addEventListener('click', showIp);











// 2 спосіб - всі дії в слухач
// const getIPButton = document.querySelector('.ip-btn');
// const ipInfoDiv = document.querySelector('.ip-info');

// getIPButton.addEventListener('click', async () => {
//   try {
//     const ipResponse = await fetch('https://api.ipify.org/?format=json');
//     const { ip } = await ipResponse.json();
    
//     const ipInfoResponse = await fetch(`http://ip-api.com/json/${ip}?fields=continent,country,regionName,city,district`);
//     const ipInfo = await ipInfoResponse.json();
    
//     const { continent, country, regionName, city, district } = ipInfo;

//     ipInfoDiv.innerHTML = `
//       <p>Континент: ${continent}</p>
//       <p>Країна: ${country}</p>
//       <p>Регіон: ${regionName}</p>
//       <p>Місто: ${city}</p>
//       <p>Район: ${district}</p>
//     `;
//   } catch (error) {
//     console.error(error);
//   }
// });